package main // import "github.com/chordflower/finance"

import (
	"os"

	cmd "github.com/chordflower/finance/cmd"
	log "github.com/kpango/glg"
	cli "github.com/urfave/cli"
)

type mainApp struct {
	args     *cli.App
	verbose  bool
	serve    cmd.CommandRunner
	generate *cmd.Generate
	logger   *log.Glg
}

func mainAppNew() *mainApp {
	return &mainApp{
		args:     nil,
		verbose:  false,
		serve:    nil,
		generate: cmd.GenerateNew(),
		logger:   log.New(),
	}
}

func (main *mainApp) configureOptions() {
	main.args = cli.NewApp()
	main.args.Authors = []cli.Author{
		{Name: "carddamom", Email: "carddamom at outlook dot pt"},
	}
	main.args.Copyright = "Copyright (C) 2018  carddamom"
	main.args.Description = "A golang rest webapp for personal finance management"
	main.args.Name = "finance"
	main.args.Version = "0.1.1"
	main.args.Commands = []cli.Command{
		main.serve.Configure(),
		cli.Command{
			Name:        "generate",
			Description: "generate a server key, cipher key or archive format",
			Usage:       "generate a server key, cipher key or archive format",
			Subcommands: []cli.Command{
				cli.Command{
					Name:        "server key",
					Aliases:     []string{"serverKey"},
					Usage:       "generate the self-signed server private/public key",
					Description: "generate the self-signed server private/public key",
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:        "output",
							Usage:       "specifies the name of the `output` file",
							Destination: &main.generate.Output,
						},
						cli.BoolTFlag{
							Name:        "append",
							Usage:       "If the output file exists, append the key to it",
							Destination: &main.generate.Append,
						},
					},
					Action: func(c *cli.Context) error {
						main.generate.Mode = cmd.ModeCertificate
						return main.generate.Execute(c)
					},
				},
				cli.Command{
					Name:        "cipher key",
					Aliases:     []string{"cipherKey"},
					Usage:       "generate the symmetric cipher key, to cipher the sensitive information of the property files",
					Description: "generate the symmetric cipher key, to cipher the sensitive information of the property files",
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:        "output",
							Usage:       "specifies the name of the `output` file",
							Destination: &main.generate.Output,
						},
						cli.BoolTFlag{
							Name:        "append",
							Usage:       "If the output file exists, append the key to it",
							Destination: &main.generate.Append,
						},
					},
					Action: func(c *cli.Context) error {
						main.generate.Mode = cmd.ModeSymmetric
						return main.generate.Execute(c)
					},
				},
				cli.Command{
					Name:        "list",
					Usage:       "lists the keys present in the file",
					Description: "lists the keys present in the file",
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:        "output",
							Usage:       "specifies the name of the `key` file",
							Destination: &main.generate.Output,
						},
					},
					Action: func(c *cli.Context) error {
						main.generate.Mode = cmd.ModeList
						return main.generate.Execute(c)
					},
				},
			},
		},
	}

	main.args.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "verbose",
			Usage:       "sets the verbose mode",
			Destination: &main.verbose,
		},
	}
}

func (main *mainApp) configureLogging() {
	main.logger.SetMode(log.STD)
}

func (main *mainApp) extractConfiguration() {

}

func (main *mainApp) run() {
	main.configureLogging()
	main.serve = cmd.ServeNew(main.logger)
	main.configureOptions()
	main.args.Action = main.serve.Execute
	err := main.args.Run(os.Args)
	if err != nil {
		main.logger.Errorf("%s", err)
	}
}

func main() {
	main := mainAppNew()
	main.run()
}
