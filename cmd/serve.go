package cmd // import "github.com/chordflower/finance/cmd"
import (
	"fmt"
	"net/http"

	alice "github.com/justinas/alice"
	nosurf "github.com/justinas/nosurf"
	handlers "github.com/kevinburke/handlers"
	log "github.com/kpango/glg"
	cli "github.com/urfave/cli"
)

// Serve contains the parsed arguments that reference the server
type Serve struct {
	Port    uint
	Address string
	Config  string
	log     *log.Glg
}

// ServeNew creates a new serve object
func ServeNew(log *log.Glg) *Serve {
	return &Serve{log: log, Port: 8080, Address: "0.0.0.0"}
}

func (serve *Serve) sample(w http.ResponseWriter, r *http.Request) {
	serve.log.Infof("Handling request from %v", r.RemoteAddr)
	w.WriteHeader(200)
	w.Write([]byte("Hello world!"))
}

// Configure the options for the serve command
func (serve *Serve) Configure() cli.Command {
	cmd := cli.Command{
		Name:        "run",
		Aliases:     []string{"serve"},
		Usage:       "runs the server",
		Description: "runs the server",
		Flags: []cli.Flag{
			cli.UintFlag{
				Name:        "port, p",
				Usage:       "defines the `port` to use",
				Value:       8080,
				Destination: &serve.Port,
			},
			cli.StringFlag{
				Name:        "address, a",
				Usage:       "defines the `address` to bind to",
				Value:       "0.0.0.0",
				Destination: &serve.Address,
			},
			cli.StringFlag{
				Name:        "config, c",
				Usage:       "defines the `configuration` file to use",
				Value:       "config.yaml",
				Destination: &serve.Config,
			},
		},
		Action: serve.Execute,
	}
	return cmd
}

// Execute the command, starting the server
func (serve *Serve) Execute(c *cli.Context) error {
	chain := alice.New(nosurf.NewPure,
		handlers.UUID,
		handlers.Duration,
		handlers.Debug,
	).Then(http.HandlerFunc(serve.sample))
	serve.log.Infof("Serving requests at %v:%d", serve.Address, serve.Port)
	http.ListenAndServe(fmt.Sprintf("%v:%d", serve.Address, serve.Port), chain)
	return nil
}
