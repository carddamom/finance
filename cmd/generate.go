package cmd // import "github.com/chordflower/finance/cmd"

import (
	cli "github.com/urfave/cli"
)

//go:generate go-enum -f=generate.go --marshal --lower

// Mode is an enumation of modes that are allowed
/*ENUM( Certificate, Symmetric, List ) */
type Mode int

// Generate is the generate command.
type Generate struct {
	Output string
	Append bool
	Mode   Mode
}

// GenerateNew create a new Generate command.
func GenerateNew() *Generate {
	return &Generate{}
}

// Configure the generate command.
func (generate *Generate) Configure() cli.Command {
	return cli.Command{}
}

func (generate *Generate) certificate() error {
	return nil
}

func (generate *Generate) symmetric() error {
	return nil
}

func (generate *Generate) list() error {
	return nil
}

// Execute the generate command, generating the certificates, keys, etc.
func (generate *Generate) Execute(c *cli.Context) error {
	switch generate.Mode {
	case ModeCertificate:
		return generate.certificate()
	case ModeSymmetric:
		return generate.symmetric()
	case ModeList:
		return generate.list()
	default:
		//Do the rest
	}
	return nil
}
