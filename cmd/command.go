package cmd

import (
	cli "github.com/urfave/cli"
)

// CommandRunner represents a command line, command
type CommandRunner interface {
	Configure() cli.Command
	Execute(c *cli.Context) error
}
