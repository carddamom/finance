package archive // import "github.com/chordflower/finance/lib/archive"

import (
	"encoding/base64"
	"time"

	badger "github.com/dgraph-io/badger"
	errors "github.com/juju/errors"
	uuid "github.com/kevinburke/go.uuid"
	json "github.com/pquerna/ffjson/ffjson"
	hash "golang.org/x/crypto/blake2b"
)

//go:generate go-enum -f=archive.go --marshal --lower

// RecordType is an enumation with the possible record types
/*ENUM( PublicKey, PrivateKey, SymKey, Data ) */
type RecordType int

// Archive defines an archive containing, private/public keys, symmetric keys and other type of data.
type Archive struct {
	ID       string
	Version  int
	Created  string
	Modified string
	Size     int
	records  []Record
	db       *badger.DB
}

func hashStr(toHash string) ([]byte, error) {
	blake, err := hash.New512(nil)
	if err != nil {
		return nil, errors.Trace(err)
	}
	data := blake.Sum([]byte("main"))
	return data, nil
}

// ArchiveNew creates a new archive file in the given directory.
func ArchiveNew(filename string) (*Archive, error) {
	opts := badger.DefaultOptions
	opts.Dir = filename
	opts.ValueDir = filename
	db, err := badger.Open(opts)
	if err != nil {
		return nil, errors.Trace(err)
	}
	archive := Archive{}
	archive.Created = time.Now().UTC().Format(time.RFC3339)
	archive.ID = uuid.NewV1().String()
	archive.Modified = archive.Created
	archive.Version = 0
	archive.db = db
	archive.Size = 0
	var key, value []byte
	key, err = hashStr("main")
	if err != nil {
		return nil, errors.Trace(err)
	}
	value, err = json.Marshal(archive)
	if err != nil {
		return nil, errors.Trace(err)
	}
	err = db.Update(func(txt *badger.Txn) error {
		err = txt.Set(key, value)
		if err != nil {
			return errors.Trace(err)
		}
		return nil
	})
	return &archive, errors.Trace(err)
}

// ReadFrom the filename containing a serialized archive.
func ReadFrom(filename string) (*Archive, error) {
	opts := badger.DefaultOptions
	opts.Dir = filename
	opts.ValueDir = filename
	db, err := badger.Open(opts)
	if err != nil {
		return nil, errors.Trace(err)
	}
	archive := Archive{}
	err = db.View(func(txt *badger.Txn) error {
		data, err := hashStr("main")
		record, err := txt.Get(data)
		if err != nil {
			return errors.Trace(err)
		}
		data, err = record.Value()
		if err != nil {
			return errors.Trace(err)
		}
		err = json.NewDecoder().Decode(data, archive)
		return errors.Trace(err)
	})
	archive.db = db
	return &archive, errors.Trace(err)
}

// Close the resources used by the archive.
func (archive *Archive) Close() {
	archive.db.Close()
}

// Write the current archive to a file.
func (archive *Archive) Write() error {
	var err error
	if archive.db != nil {
		var key, value []byte
		key, err := hashStr("main")
		if err != nil {
			return errors.Trace(err)
		}
		value, err = json.Marshal(archive)
		if err != nil {
			return errors.Trace(err)
		}
		err = archive.db.Update(func(txt *badger.Txn) error {
			err = txt.Delete(key)
			if err != nil {
				return errors.Trace(err)
			}
			err = txt.Set(key, value)
			if err != nil {
				return errors.Trace(err)
			}
			return nil
		})
	}
	return errors.Trace(err)
}

func (archive *Archive) getRecordKey(recordType RecordType, name string) ([]byte, error) {
	blake, err := hash.New512(nil)
	if err != nil {
		return nil, errors.Trace(err)
	}
	_ = blake.Sum([]byte(name))
	data := blake.Sum([]byte(recordType.String()))
	return data, nil
}

func (archive *Archive) findRecordByTypeAndName(recordType RecordType, name string) (*Record, error) {
	if archive.db != nil {
		data, err := archive.getRecordKey(recordType, name)
		if err != nil {
			return nil, errors.Trace(err)
		}
		err = archive.db.View(func(txt *badger.Txn) error {
			item, err := txt.Get(data)
			if err != nil {
				return errors.Trace(err)
			}
			data, err = item.Value()
			if err != nil {
				return errors.Trace(err)
			}
			return nil
		})
		if err != nil {
			return nil, errors.Trace(err)
		}
		record := Record{}
		if data != nil {
			err = json.Unmarshal(data, record)
			if err != nil {
				return nil, errors.Trace(err)
			}
			return &record, nil
		}
	}
	return nil, errors.NotFoundf("The given record wasn't found")
}

// FindCertificateByName returns the certificate binary data, given its name in the archive.
func (archive *Archive) FindCertificateByName(name string) ([]byte, error) {
	record, err := archive.findRecordByTypeAndName(RecordTypePublicKey, name)
	if err != nil {
		return nil, errors.Trace(err)
	}
	data, err := record.Data()
	return data, errors.Trace(err)
}

// FindPrivateKeyByName returns the private binary data, given its name in the archive.
func (archive *Archive) FindPrivateKeyByName(name string) ([]byte, error) {
	record, err := archive.findRecordByTypeAndName(RecordTypePrivateKey, name)
	if err != nil {
		return nil, errors.Trace(err)
	}
	data, err := record.Data()
	return data, errors.Trace(err)
}

// FindSymKeyByName returns the symmetric key binary data, given its name in the archive.
func (archive *Archive) FindSymKeyByName(name string) ([]byte, error) {
	record, err := archive.findRecordByTypeAndName(RecordTypeSymKey, name)
	if err != nil {
		return nil, errors.Trace(err)
	}
	data, err := record.Data()
	return data, errors.Trace(err)
}

// AddCertificate adds the given certificate with the given name to the archive.
func (archive *Archive) AddCertificate(certificate []byte, name string, algorithm string) error {
	var key, value []byte
	var err error
	key, err = archive.getRecordKey(RecordTypePublicKey, name)
	if err != nil {
		return errors.Trace(err)
	}
	record := Record{}
	record.Algorithm = algorithm
	record.Comments = ""
	record.Created = time.Now().UTC().Format(time.RFC3339)
	record.ID = uuid.NewV5(uuid.NewV1(), name).String()
	record.Modified = record.Created
	record.Name = name
	record.Type = RecordTypePublicKey
	record.Version = 0
	record.SetData(certificate)
	value, err = json.Marshal(record)
	if err != nil {
		return errors.Trace(err)
	}
	if archive.db != nil {
		err = archive.db.Update(func(txt *badger.Txn) error {
			_, err = txt.Get(key)
			if err == nil {
				return errors.AlreadyExistsf("The record already exists.")
			}
			err = txt.Set(key, value)
			if err != nil {
				return errors.Trace(err)
			}
			return nil
		})
		if err != nil {
			return errors.Trace(err)
		}
		archive.Size = archive.Size + 1
		return nil
	}
	return errors.NotValidf("The current archive is not valid.")
}

// AddPrivateKey adds the given private key with the given name to the archive.
func (archive *Archive) AddPrivateKey(private []byte, name string, algorithm string) error {
	var key, value []byte
	var err error
	key, err = archive.getRecordKey(RecordTypePrivateKey, name)
	if err != nil {
		return errors.Trace(err)
	}
	record := Record{}
	record.Algorithm = algorithm
	record.Comments = ""
	record.Created = time.Now().UTC().Format(time.RFC3339)
	record.ID = uuid.NewV5(uuid.NewV1(), name).String()
	record.Modified = record.Created
	record.Name = name
	record.Type = RecordTypePrivateKey
	record.Version = 0
	record.SetData(private)
	value, err = json.Marshal(record)
	if err != nil {
		return errors.Trace(err)
	}
	if archive.db != nil {
		err = archive.db.Update(func(txt *badger.Txn) error {
			_, err = txt.Get(key)
			if err == nil {
				return errors.AlreadyExistsf("The record already exists.")
			}
			err = txt.Set(key, value)
			if err != nil {
				return errors.Trace(err)
			}
			return nil
		})
		if err != nil {
			return errors.Trace(err)
		}
		archive.Size = archive.Size + 1
		return nil
	}
	return errors.NotValidf("The current archive is not valid.")
}

// AddSymKey adds the given symmetric key with the given name to the archive.
func (archive *Archive) AddSymKey(sym []byte, name string, algorithm string) error {
	var key, value []byte
	var err error
	key, err = archive.getRecordKey(RecordTypeSymKey, name)
	if err != nil {
		return errors.Trace(err)
	}
	record := Record{}
	record.Algorithm = algorithm
	record.Comments = ""
	record.Created = time.Now().UTC().Format(time.RFC3339)
	record.ID = uuid.NewV5(uuid.NewV1(), name).String()
	record.Modified = record.Created
	record.Name = name
	record.Type = RecordTypeSymKey
	record.Version = 0
	record.SetData(sym)
	value, err = json.Marshal(record)
	if err != nil {
		return errors.Trace(err)
	}
	if archive.db != nil {
		err = archive.db.Update(func(txt *badger.Txn) error {
			_, err = txt.Get(key)
			if err == nil {
				return errors.AlreadyExistsf("The record already exists.")
			}
			err = txt.Set(key, value)
			if err != nil {
				return errors.Trace(err)
			}
			return nil
		})
		if err != nil {
			return errors.Trace(err)
		}
		archive.Size = archive.Size + 1
		return nil
	}
	return errors.NotValidf("The current archive is not valid.")
}

// Record is a single record of a archive, and contains an private key, public key, symmetric key or other type of data.
type Record struct {
	ID        string
	Name      string
	Version   int
	Created   string
	Modified  string
	Type      RecordType
	Algorithm string
	Comments  string
	data      string
}

// Data returns the decoded binary data available in this record.
func (record *Record) Data() ([]byte, error) {
	data, err := base64.StdEncoding.DecodeString(record.data)
	return data, errors.Trace(err)
}

// SetData sets the unencoded binary data as the encoded data in this record.
func (record *Record) SetData(unencoded []byte) {
	record.data = base64.StdEncoding.EncodeToString(unencoded)
}
